<?php
namespace PhalconProjectForForum\User\Controllers;

use Phalcon\Mvc\View;

class LoginController extends \Phalcon\Mvc\Controller
{
    public function loginAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
    
    public function loginUserAction()
    {       
            $this->view->disable();
            
            return $this->response->redirect("main/proceed");
        
    }
}
