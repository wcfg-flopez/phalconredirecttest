<?php
namespace PhalconProjectForForum\User;

class Module
{

	public function registerAutoloaders()
	{

		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'PhalconProjectForForum\User\Controllers' => __DIR__.'/controllers/',
			'PhalconProjectForForum\Common\Models' => __DIR__.'/../../common/models/',
		));
        
        $loader->registerDirs(array(
            '../../common/models/'
            ));

		$loader->register();
	}

	public function registerServices($di)
	{

		/**
		 * Setting up the view component
		 */
		$di->set('view', function() {

			$view = new \Phalcon\Mvc\View();

			$view->setViewsDir(__DIR__.'/views/');
           
			return $view;

		});

	}

}