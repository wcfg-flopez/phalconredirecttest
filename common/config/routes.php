<?php

$router = new \Phalcon\Mvc\Router(false);

$router->removeExtraSlashes(true);

/**
 * Frontend routes
 */

$router->add('', array(
    'module' => 'User',
    'namespace' => 'PhalconProjectForForum\User\Controllers\\',
    'controller' => 'main',
    'action' => 'main'
));

$router->add('/', array(
    'module' => 'User',
    'namespace' => 'PhalconProjectForForum\User\Controllers\\',
    'controller' => 'main',
    'action' => 'main'
));

$router->add('/:controller', array(
        'module' => "User",
        'namespace' => 'PhalconProjectForForum\User\Controllers\\',
        'controller' =>1,
        'action' => 1
    ));

$router->add('/:controller/:action', array(
        'module' => "User",
        'namespace' => 'PhalconProjectForForum\User\Controllers\\',
        'controller' =>1,
        'action' => 2
    ));

$router->notFound(array(
	'module' => 'User',
	'namespace' => 'PhalconProjectForForum\User\Controllers\\',
	'controller' => 'main',
	'action' => 'route404'
));

return $router;

?>