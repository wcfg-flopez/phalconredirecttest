<?php

namespace PhalconProjectForForum\Common\Models;

class taxonomy extends \Phalcon\Mvc\Model
{
    //public $taxonomyID;
    
    //public $name;
    
    public function getSource()
    {
        return "taxonomy";
    }
    
    public function initialize()
    {
       // $this->hasOne("taxonomyID", "PhalconProjectForForum\Common\Models\items", "taxonomyId");
       $this->belongsTo("taxonomyID", "PhalconProjectForForum\Common\Models\items", "taxonomyId");
       $this->hasMany("taxonomyID", "PhalconProjectForForum\Common\Models\items", "taxonomyId");
    }
}
