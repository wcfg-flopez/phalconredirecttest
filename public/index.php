<?php
    try{
        // create Dependency Injection
        $di = new \Phalcon\DI\FactoryDefault();
        
        // set the router
        $di->set('router', require __DIR__ . '/../common/config/routes.php');
        
        // set the base url
        $di->set('url', function() {
            $url = new \Phalcon\Mvc\Url();
            $url->setBaseUri('/PhalconProjectForForum/'); // If the project is in the Document Root folder, setBaseUri to '/'
            return $url;
        });
        
        // set up the models Manager
        $di->set('modelsManager', function(){
            return new Phalcon\Mvc\Model\Manager();
        });
        
        // create the application object
        $application = new \Phalcon\Mvc\Application();
        
        // Set the Dependency Injection object to the application
        $application->setDI($di);
        
        // register modules with the application
        $application->registerModules(array(
            'User' => array(
                        'className' => 'PhalconProjectForForum\User\Module',
                        'path'  => '../apps/user/Module.php'
                   )
        ));
        
        $handle = $application->handle();
        
        $retStr = $handle->getContent(); 
         
        echo $retStr;
    }
    catch (Phalcon\Exception $e) 
    {
	    echo $e->getMessage();
    } 
    catch (PDOException $e)
    {
	    echo $e->getMessage();
    }
?>